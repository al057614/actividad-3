package Programa_5;
import static java.lang.System.out;
import java.util.Scanner;
class Equipo {
private String nombre;
private int juegosJugados;
private int juegosGanados;
private int juegosPerdidos;
private int golesFavor;
private int golesEnContra;
public int calcularPuntos() {
return juegosJugados * 3;
}
public int calcularBono() {
return calcularPuntos() * 100
+ golesFavor * 500
- juegosPerdidos * 500
+ (juegosJugados % 2 == 0 ? 5000 : 0);
}
@Override
public String toString() {
return String.format("Nombre: %-20s, Bono: %-10d, Puntos: %-10d", nombre, calcularBono(),
calcularPuntos());
}
public static Equipo creaEquipo( String nombre, int jj, int jg, int jp, int gf, int ge ) {
Equipo e = new Equipo();
e.nombre = nombre;
e.juegosJugados = jj;
e.juegosGanados = jg;
e.juegosPerdidos = jp;
e.golesFavor = gf;
e.golesEnContra = ge;
return e;

}
}
public class TORNEOFI {
private static final Scanner in = new Scanner(System.in);
public static void main( String ... args ) {
int numeroEquipos = readInt("Escriba el numero de equipos: ");
Equipo [] equipos = new Equipo[numeroEquipos];
for ( int i = 0 ; i < numeroEquipos ; i++ ) {
equipos[i] = Equipo.creaEquipo(
readString("\n\nNombre del equipo: "),
readInt("Partidos jugados: "),
readInt("Partidos ganados: "),
readInt("Partidos perdidos: "),
readInt("Goles a favor: "),
readInt("Goles en contra: "));
}
for ( Equipo e : equipos ) {
out.println( e );
}
}
private static String readString( String message ) {
System.out.print( message );
return in.next();
}
private static int readInt(String message) {
System.out.print(message);
return in.nextInt();
}
}

