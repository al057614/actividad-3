package actividad.pkg3.arreglos.multidimensionales;

public class Programa_1 {

    public static void main(String[] args) {
             String[][] CODIGO;
        CODIGO = new String[26][2];
        // Arreglo con representación del Alfabeto
        CODIGO[0][0] = "A";
        CODIGO[1][0] = "B";
        CODIGO[2][0] = "C";
        CODIGO[3][0] = "D";
        CODIGO[4][0] = "E";
        CODIGO[5][0] = "F";
        CODIGO[6][0] = "G";
        CODIGO[7][0] = "H";
        CODIGO[8][0] = "I";
        CODIGO[9][0] = "J";
        CODIGO[10][0] = "K";
        CODIGO[11][0] = "L";
        CODIGO[12][0] = "M";
        CODIGO[13][0] = "N";
        CODIGO[14][0] = "O";
        CODIGO[15][0] = "P";
        CODIGO[16][0] = "Q";
        CODIGO[17][0] = "R";
        CODIGO[18][0] = "S";
        CODIGO[19][0] = "T";
        CODIGO[20][0] = "U";
        CODIGO[21][0] = "V";
        CODIGO[22][0] = "W";
        CODIGO[23][0] = "X";
        CODIGO[24][0] = "Y";
        CODIGO[25][0] = "Z";
        
        // En código Morse
        CODIGO[0][1] = ".-";
        CODIGO[1][1] = "-...";
        CODIGO[2][1] = "-.-.";
        CODIGO[3][1] = "-..";
        CODIGO[4][1] = ".";
        CODIGO[5][1] = "..-.";
        CODIGO[6][1] = "--.";
        CODIGO[7][1] = "...";
        CODIGO[8][1] = "..";
        CODIGO[9][1] = ".---";
        CODIGO[10][1] = "-.-";
        CODIGO[11][1] = ".-..";
        CODIGO[12][1] = "--";
        CODIGO[13][1] = "-.";
        CODIGO[14][1] = "---";
        CODIGO[15][1] = ".--.";
        CODIGO[16][1] = "--.-";
        CODIGO[17][1] = ".-.";
        CODIGO[18][1] = "...";
        CODIGO[19][1] = "-";
        CODIGO[20][1] = "..-";
        CODIGO[21][1] = "...-";
        CODIGO[22][1] = ".--";
        CODIGO[23][1] = "-..-";
        CODIGO[24][1] = "-.--";
        CODIGO[25][1] = "--..";          
        
        System.out.println("=======================");
        for (int i=0;i <26; i++){
            System.out.println(CODIGO[i][0]+ " = "+ CODIGO[i][1]);

        
    }
    }
    
}

